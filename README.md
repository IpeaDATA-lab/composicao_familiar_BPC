# Complementação dos grupos familiares do BPC com informações do Cadastro Único 

# Diretórios
* **programas:** diretório onde estarão as rotinas em R que reproduzem os resultados da nota técnica
* **resultados:** diretório onde estarão armazenadas a nota técnica em pdf e demais suplementos necessários para reproduzir a nota em R Markdown (.rmd)
* **tabelas_reclassificacao:** diretório onde estarão armazenadas as tabelas com as regras necessárias para a recriação do grupamento familiar do beneficiário do BPC dentro do Cadastro Único
