
##### Functions to open and cleaning PNAD Continua #########

  # if required install and library packages
    open_install_package <- function(x){
      for( i in x ){
        if( ! require( i , character.only = TRUE ) ){
          install.packages( i , dependencies = TRUE )
          require( i , character.only = TRUE )
        }
      }
    }
    
    
    
    # gen merge variable
    gen_merge <- function(data, bpc, cad){
      data[ !is.na(get(bpc)) & !is.na(get(cad)),  merge_:=3] # match
      data[ !is.na(get(bpc)) &  is.na(get(cad)),  merge_:=1] # GRUFAM
      data[  is.na(get(bpc)) & !is.na(get(cad)),  merge_:=2] # cad
    }
    
    unifyVar_merge<- function(ds, variable_list){
      for (i in variable_list) {
        print(i)
        ds[, eval(i) := ifelse( merge_==1, get(paste0(i, ".x")),
                                ifelse(merge_==2 | merge_==3,   get(paste0(i, ".y")), ""))]
      }
    }
    
    
    abrevia_nomes_meio<- function(nome){
      first<- str_extract(nome, "^[A-Z]+(?=\\s)")
      last<- str_extract(nome, "[A-Z]+$")
      mid<- str_extract(nome, "\\s[A-Z]+\\s")
      mid<- ifelse(is.na(mid),"",mid) %>%
        str_trim() %>%
        str_replace_all("(?<=[A-Z])[A-Z]", "")
      nome_corrigido<- paste(first,mid,last, sep = " ") %>% str_replace_all("\\s+"," ")
      return(nome_corrigido)
    }
    
    
    
############## Tabelas de resultados #################################
    
    # TABELA de reclassificacoes corretas e pares de erros
    tab_acuracia <- function(){
      acerto_reclass<- GRUFAM_cad_tab_ext[merge_==3, .(cpf, nb, nome, cs_vinc_familiar_BPC, cs_vinc_familiar_cad)]
      acerto_reclass[, cs_vinc_familiar_BPC:=as.numeric(cs_vinc_familiar_BPC)][, cs_vinc_familiar_cad:=as.numeric(cs_vinc_familiar_cad)]
      acerto_reclass[cs_vinc_familiar_BPC==6, cs_vinc_familiar_BPC:=1 ]
      acerto_reclass[cs_vinc_familiar_BPC==8, cs_vinc_familiar_BPC:=2 ]
      acerto_reclass[cs_vinc_familiar_BPC==0, cs_vinc_familiar_BPC:=10 ]
      acerto_reclass[, acerto:= (cs_vinc_familiar_BPC - cs_vinc_familiar_cad)]
      acerto_reclass[acerto!=0, acerto:=99]
      
      tab_acerto<- acerto_reclass[, .(N.pessoas=.N),
                                         by= acerto][, acerto:=ifelse(acerto==0, "Correta", "Errada")][, '%':= round(N.pessoas/sum(N.pessoas)*100,2)]
      # tab_pares_erro <- acerto_reclass[acerto==99, .(Total_de_ocorrência=.N), by=.(Vínculo_GRUFAM=cs_vinc_familiar_BPC,
      #                                                                          Vínculo_reclassificado=cs_vinc_familiar_cad)][order(-Total_de_ocorrência)]
    }
    
    
    # TABELA de proporcao reclassificada e nao reclassificada e motivo de nao reclassificar
      
    tab_reclas_motivos <- function(){
        aux <-GRUFAM_cad_tab[, .('Reclassificações' = ifelse(cs_vinc_familiar!=99  & !is.na(cs_vinc_familiar), 'Reclassificado','Não reclassificado'))
                                                              ][, .(N.pessoas=.N), 'Reclassificações'][order(N.pessoas)][, '(%)':= round(N.pessoas/sum(N.pessoas)*100,2)]
        aux1 <-GRUFAM_cad_tab[cs_vinc_familiar!=99  & !is.na(cs_vinc_familiar), .(N.pessoas = .N) , (Reclassificações=(cs_vinc_familiar!=0))][,
                                                                                          '(%)':= round(N.pessoas/sum(N.pessoas)*100,2)]
        aux1[, Reclassificações:= as.character(Reclassificações)][Reclassificações=='TRUE', Reclassificações:= 'Entra na família'][
          Reclassificações=='FALSE', Reclassificações:= 'Não entra na família']
        aux3 <- GRUFAM_cad_tab[cs_vinc_familiar==99  | is.na(cs_vinc_familiar), .(N.pessoas=.N), .((cs_vinc_familiar==99), solteiro)]
        
        aux3[cs_vinc_familiar==TRUE, Reclassificações := 'Ambíguo' ][is.na(cs_vinc_familiar) & is.na(solteiro),
                                                                     Reclassificações:='Estado civil nao identificado'][is.na(Reclassificações), Reclassificações:= 'Não Parentes e Outros parentes ou demais relações com essas posições']
        aux3 <-aux3[,.(N.pessoas=sum(N.pessoas)), Reclassificações][, '(%)':= round(N.pessoas/sum(N.pessoas)*100,2)]
        aux4<-rbind(aux,aux1,aux3)
        GRUFAM_cad_tab<-merge(GRUFAM_cad_tab, grufam[cs_vinc_familiar=="10"|cs_vinc_familiar=="00", .(nb, especie_do_beneficio)], by ='nb',
                              all.x = T)
        for (i in c(87,88)) {
          aux <-GRUFAM_cad_tab[especie_do_beneficio==i, .('Reclassificações' = ifelse(cs_vinc_familiar!=99  & !is.na(cs_vinc_familiar), 'Reclassificado','Não reclassificado'))
                               ][, .(N.pessoas=.N), 'Reclassificações'][order(N.pessoas)][, '(%)':= round(N.pessoas/sum(N.pessoas)*100,2)]
          aux1 <-GRUFAM_cad_tab[cs_vinc_familiar!=99  & !is.na(cs_vinc_familiar) & especie_do_beneficio==i, .(N.pessoas = .N) , (Reclassificações=(cs_vinc_familiar!=0))][,
                                                                                                                                                                          '(%)':= round(N.pessoas/sum(N.pessoas)*100,2)]
          aux1[, Reclassificações:= as.character(Reclassificações)][Reclassificações=='TRUE', Reclassificações:= 'Entra na família'][
            Reclassificações=='FALSE', Reclassificações:= 'Não entra na família']
          aux3 <- GRUFAM_cad_tab[(cs_vinc_familiar==99  | is.na(cs_vinc_familiar)) & especie_do_beneficio==i, .(N.pessoas=.N), .((cs_vinc_familiar==99), solteiro)]
          
          aux3[cs_vinc_familiar==TRUE, Reclassificações := 'Ambíguo' ][is.na(cs_vinc_familiar) & is.na(solteiro),
                                                                       Reclassificações:='Estado civil nao identificado'][is.na(Reclassificações), Reclassificações:= 'Não Parentes e Outros parentes ou demais relações com essas posições']
          aux3 <-aux3[,.(N.pessoas=sum(N.pessoas)), Reclassificações][, '(%)':= round(N.pessoas/sum(N.pessoas)*100,2)]
          assign(paste0('aux4',eval(i)), rbind(aux,aux1,aux3))
        }
      final<-merge(aux4,merge(aux487, aux488, by = 'Reclassificações', all = T) , by='Reclassificações', all = T)
    }
    
    # tabelas merge final by:chaves
    tab_mergeFim1 <- function(){ 
      c("1º por CPF, NB e data de nascimento",nrow(merge(grufam,GRUFAM_cad_tab,
                                                                           by=c("cpf", "nb", "data_de_nascimento"), nomatch=0)))
    }
    
    tab_mergeFim2 <- function(){ 
      c("2º por CPF, NB e nome abreviado sem D.",nrow(merge(GRUFAM_cad_tab_ext_BPC,GRUFAM_cad_tab_ext_reclas
                                                                              ,by=c("cpf", "nb", "nome_abrev_semD"), nomatch=0)))
    }
    tab_mergeFim3 <- function(){ 
      c("3º por Nome, NB e data de nascimento",nrow(merge(GRUFAM_cad_tab_ext_BPC,GRUFAM_cad_tab_ext_reclas
                                                                              ,by=c("nome", "nb", "data_de_nascimento"), nomatch=0)))
    }
    tab_mergeFim4 <- function(){ 
      c("4º por Nome fonético, NB e data de nascimento",nrow(merge(GRUFAM_cad_tab_ext_BPC, GRUFAM_cad_tab_ext_reclas
                                                                              ,by=c("nome_metaphone", "nb", "data_de_nascimento"), nomatch=0)))
    }
    tab_mergeFim5 <- function(){ 
      c("5º por Nome e NB",nrow(merge(GRUFAM_cad_tab_ext_BPC,GRUFAM_cad_tab_ext_reclas
                                                                              ,by=c("nome", "nb"), nomatch=0)))
    }
    tab_mergeFim6 <- function(){ 
      c("6º por Nome fonético e NB",nrow(merge(GRUFAM_cad_tab_ext_BPC,GRUFAM_cad_tab_ext_reclas
                                                                              ,by=c("nb", "data_de_nascimento"), nomatch=0)))
    }
    tab_mergeFim7 <- function(){ 
      c("7º por NB e data de nascimento",nrow(merge(GRUFAM_cad_tab_ext_BPC,GRUFAM_cad_tab_ext_reclas
                                                                              ,by=c("nome_metaphone", "nb"), nomatch=0)))
    }
      #agrupa
      tab_merge_byKey<- function(){
        tab_merge_Key<-rbind(tab_1_pareamento,tab_2_pareamento,tab_3_pareamento,tab_4_pareamento,
                             tab_5_pareamento,tab_6_pareamento,tab_7_pareamento)
      }
    
    # tabela final grufam_ext by merge_
      tab_merge<- function(){
        tab_merge_fim<-GRUFAM_cad_tab_ext[, .N, by=merge_][order(merge_)]
        tab_merge_fim<- tab_merge_fim[c(3,2,1),]
        tab_merge_fim[,merge_:=as.character(merge_)]
        tab_merge_fim[merge_==1,merge_:="Só GRUFAM original"]
        tab_merge_fim[merge_==3,merge_:="GRUFAM e GRUFAM-CAD"]
        tab_merge_fim[merge_==2,merge_:="Só GRUFAM-CAD"]
        names(tab_merge_fim)<-c("Resultado do Pareamento", "Número de observações")
        total<-as.data.frame(t(c("Total",round(colSums(tab_merge_fim[,-1], na.rm = T)))))
        names(total)<-names(tab_merge_fim)
        tab_merge_fim<-rbind(tab_merge_fim, total)
      }
      
# grafico subnotificacao por dib
      graf_dib<-function(){
        grufam[        , nb:= as.character(nb)]
        GRUFAM_cad_tab[, nb:= as.character(nb)]
        
        grufam_filter<- grufam[GRUFAM_cad_tab[oproprio_benef==1, .(nb)], on='nb', nomatch=0]
        grufam_filter[            , contador:=1][cs_vinc_familiar=="10"|cs_vinc_familiar=="00", oproprio_benef:=1]
        grufam_filter[, `:=` (ano_dib = substr(as.character(dib),1,4 ), mes_dib = as.numeric(substr(as.character(dib),5,6 )))]
        grufam_filter[, trimestre_dib:= ifelse(between(mes_dib, 1,3),'1', 
                                               ifelse(between(mes_dib, 4,6), '2', 
                                                      ifelse(between(mes_dib, 7,9), '3',
                                                             ifelse(between(mes_dib, 10,12), '4', NA))))]
        
        GRUFAM_cad_tab_filter<- merge(GRUFAM_cad_tab[cs_vinc_familiar!='99' & !is.na(cs_vinc_familiar) & cs_vinc_familiar!='00',],
                                      grufam[cs_vinc_familiar=="10"|cs_vinc_familiar=="00", .(nb, dib)], by = 'nb', all.x=T)
        
        GRUFAM_cad_tab_filter[            , contador:=1]
        GRUFAM_cad_tab_filter[, `:=` (ano_dib = substr(as.character(dib.y),1,4 ), mes_dib = as.numeric(substr(as.character(dib.y),5,6 )))]
        GRUFAM_cad_tab_filter[, trimestre_dib:= ifelse(between(mes_dib, 1,3),'1', 
                                                    ifelse(between(mes_dib, 4,6), '2', 
                                                           ifelse(between(mes_dib, 7,9), '3',
                                                                  ifelse(between(mes_dib, 10,12), '4', NA))))]
        
        aux1 <- grufam_filter[,         .(GRUFAM     = sum(contador) / sum(oproprio_benef, na.rm = T) ),ano_dib][order(ano_dib)]
        aux2 <- GRUFAM_cad_tab_filter[, .(GRUFAM_CAD = sum(contador) / sum(oproprio_benef, na.rm = T) ),ano_dib][order(ano_dib)]
        
        graf_dib<-setDT(merge(aux1, aux2, by = 'ano_dib', all = T))
        graf_dib<-graf_dib[, ano_dib:= as.numeric(ano_dib)]
        graf_dib<-graf_dib[ano_dib>=1996,]
        
        theme_set(theme_bw())
        graf_dib_ggplot<-ggplot(data=graf_dib,
                                aes(x = ano_dib)) + 
          geom_line(aes(y=GRUFAM, colour="GRUFAM")        , size=1, group = 1) +
          geom_line(aes(y=GRUFAM_CAD, colour="GRUFAM-CAD"), size=1, group = 1) +
          labs(x = "Ano de inicio do benefício", y = "Média de pessoas nas famílias")+
          labs(color='Base de dados:', caption="Fonte: GRUFAM e Cadastro Único de janeiro de 2018. Elaboração própria")+
          theme(legend.position = "bottom", legend.title = element_text(face = "bold")) +
          scale_x_continuous(breaks = c(1996,2004,2009,2018))+
          scale_y_continuous(limits=c(1,2.2)) +ggtitle("Média de pessoas nas famílias por ano de início do Benefício \n Famílias de beneficiários encontrados no Cad. Único")
        
        print(graf_dib_ggplot)
        ggsave(filename = file.path(dir_repository, "resultados","graf_dib_ggplot.png") )
          
      }
    
    # histograma numero de pessoas por familia
    
    hist_med <- function(){
    cad_fam_agregado<- rbind(GRUFAM_cad_tab_ext[merge_==1 | merge_==3, .(n_pessoas_fam=.N) , by=nb
                                    ][, .(N=.N, fonte="GRUFAM  original"), by=n_pessoas_fam][order(n_pessoas_fam)] ,
                             GRUFAM_cad_tab_ext[, .(n_pessoas_fam=.N, fonte="GRUFAM estendido" ),
                                    by=nb][, .(N=.N, fonte="GRUFAM estendido"), by=n_pessoas_fam][order(n_pessoas_fam)])
    
    cad_fam_agregado[n_pessoas_fam>=6 & fonte=="GRUFAM original", N:=sum(N)]
    cad_fam_agregado[n_pessoas_fam>=6 & fonte=="GRUFAM estendido", N:=sum(N)]
    cad_fam_agregado<-cad_fam_agregado[n_pessoas_fam<=6]
    
    cad_fam_agregado[, proporcao:=round((N/sum(N)*100),1), by=fonte]
    
    
    hist_pessoa_fam<- ggplot(cad_fam_agregado, aes(x=n_pessoas_fam, y=proporcao, fill=factor(fonte))) +
      geom_bar(stat="identity", position="dodge")+labs(x="Número de pessoas na família", y="Porcentagem de famílias (%)",
                                                       fill='') +theme_minimal()+theme(legend.position="bottom")+scale_fill_grey()+
      scale_x_continuous(breaks = 1:6, labels = c("1","2","3","4","5","6+")) +
      scale_y_continuous(breaks = round(seq(min(0), max(70), by = 10),0))
    print(hist_pessoa_fam)
    
    ggsave(filename = file.path(dir_repository, "resultados","hist_pessoa_fam.png") )
    }
    
    
# numero medio de pessoas dentro das familias
    
  tab_med_total<-function(ds){
    grufam[        ,`:=` (nb = as.character(nb), cs_vinc_familiar = as.character(cs_vinc_familiar))]
    grufam[cs_vinc_familiar==0, cs_vinc_familiar:=10][, cs_vinc_familiar:= as.character(cs_vinc_familiar)]
    
    GRUFAM_cad_tab[    ,`:=` (nb = as.character(nb), cs_vinc_familiar = as.character(cs_vinc_familiar))]
    GRUFAM_cad_tab_ext[,`:=` (nb = as.character(nb), cs_vinc_familiar = as.character(cs_vinc_familiar))
                       ]
    grufam_filter            <- grufam[GRUFAM_cad_tab[cs_vinc_familiar==10            , .(nb)], on='nb', nomatch=0]
    GRUFAM_cad_tab_ext_filter<- GRUFAM_cad_tab_ext[GRUFAM_cad_tab[cs_vinc_familiar==10, .(nb)], on='nb', nomatch=0]
    
    tot_fam <- as.numeric( ds[cs_vinc_familiar==10, .(tot=.N), ])
    aux <- ds[, .('Média'=round(.N/tot_fam,1)), cs_vinc_familiar][cs_vinc_familiar!='10', ][, `:=` 
                        ('Vínculo familar' = ifelse(cs_vinc_familiar=='1', 'Cônjuge ou companheiro', 
                                                    ifelse(cs_vinc_familiar=='2','Filho ou enteado', 
                                                           ifelse(cs_vinc_familiar=='3', 'Pai/Mãe',
                                                                  ifelse(cs_vinc_familiar=='4', "Irmão/irmã",NA)))))][,
                                                                                                                      cs_vinc_familiar:=NULL][!is.na(`Vínculo familar`),]
    aux1<-ds[, .('Média'=round(.N/tot_fam,1), 'Vínculo familar'='Pessoas')]
    rbind(aux1, aux)
  }
  tab_med_especie<-function(ds, cod_especie){
    grufam[        ,`:=` (nb = as.character(nb), cs_vinc_familiar = as.character(cs_vinc_familiar))]
    grufam[cs_vinc_familiar==0, cs_vinc_familiar:=10][, cs_vinc_familiar:= as.character(cs_vinc_familiar)]
    GRUFAM_cad_tab[,`:=` (nb = as.character(nb), cs_vinc_familiar = as.character(cs_vinc_familiar))]
    
    grufam_filter            <- grufam[GRUFAM_cad_tab[cs_vinc_familiar==10            , .(nb)], on='nb', nomatch=0]
    GRUFAM_cad_tab_ext_filter<- GRUFAM_cad_tab_ext[GRUFAM_cad_tab[cs_vinc_familiar==10, .(nb)], on='nb', nomatch=0]
    
    tot_fam <- as.numeric( ds[cs_vinc_familiar==10, ESP_BENEFICIO_FAM==cod_especie, .(tot=.N), ])
    aux <- ds[ESP_BENEFICIO_FAM==cod_especie, .('Média'=round(.N/tot_fam,1)), cs_vinc_familiar][cs_vinc_familiar!='10', ][, `:=` 
                        ('Vínculo familar' = ifelse(cs_vinc_familiar=='1', 'Cônjuge ou companheiro', 
                                                    ifelse(cs_vinc_familiar=='2','Filho ou enteado', 
                                                           ifelse(cs_vinc_familiar=='3', 'Pai/Mãe',
                                                                  ifelse(cs_vinc_familiar=='4', "Irmão/irmã",NA)))))][,
                                                                                                                      cs_vinc_familiar:=NULL][!is.na(`Vínculo familar`),]
    aux1<-ds[, .('Média'=round(.N/tot_fam,1), 'Vínculo familar'='Pessoas')]
    rbind(aux1, aux)
  }
    
    